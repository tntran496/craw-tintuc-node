import NewsInfos from "../model/NewsModel.js";

export default class NewsRepo {
  constructor() {}
  findByCate = async (cateId, { offset, limit }) =>
    await NewsInfos.findAll({
      where: {
        CateId: cateId,
      },
      offset: offset,
      limit: limit,
    });
  findById = async (newsId) =>
    await NewsInfos.findAll({
      where: {
        id: newsId,
      },
    });
  findByTitle = async (title, { offset, limit }) =>
    await NewsInfos.findAll({
      where: {
        title: title,
      },
      offset: offset,
      limit: limit,
    });
}
