import cheerio from 'cheerio'
import request from 'request-promise'
import {noloadEnum,urlVnEx} from '../constant/Constant.js'
import Cates from '../model/CatesModel.js'

async function crawCate() {
    try{
        const cates = await Cates.findAll()
        if(cates.length <= 0){
            const html = await request.get(urlVnEx)
    
            let $ = cheerio.load(html.toString())
        
            $('.parent').children().each(async (index, el) => {
                let dataid = $(el).attr('data-id')
                if (!isNaN(dataid) && dataid != noloadEnum.GOC_NHIN && dataid != noloadEnum.TRANG_CHU) {
                    let url = $(el).find('a').attr('href')
                    let name = $(el).find('a').text().trim()
                    console.log(name)
                    await Cates.create({
                        id: dataid,
                        name: name,
                        url: url
                    })
                }
            })
        }
    }catch(error){
        console.log(error.message)
    }
}
crawCate()