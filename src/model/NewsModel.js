import pkg from "sequelize";
import { sequelize } from "../config/ConfigMySql.js";

const { Model, DataTypes } = pkg

export default class NewsInfos extends Model {}
NewsInfos.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT,
    },
    thumbnail: {
      type: DataTypes.STRING(1024),
    },
    detail: {
      type: DataTypes.TEXT
    },
    CateId: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: sequelize,
    modelName: "NewsInfos",
    charset: "utf8",
    collate: "utf8_unicode_ci",
    timestamp: true,
  }
);
