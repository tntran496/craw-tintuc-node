import pkg  from "sequelize";
import { sequelize } from "../config/ConfigMySql.js";
import NewsInfos from "./NewsModel.js";

const { DataTypes, Model } = pkg

export default class Cates extends Model {}
Cates.init(
  {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: {
      type: DataTypes.TEXT,
    },
    url: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: sequelize,
    charset: "utf8",
    collate: "utf8_unicode_ci",
    modelName: "Cates",
    timestamps: false,
  }
);
Cates.hasMany(NewsInfos, { as: "CateId" });
NewsInfos.belongsTo(Cates);
