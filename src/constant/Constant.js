const noloadEnum = Object.freeze({
  GOC_NHIN: "1003450",
  TRANG_CHU: "1003834",
});

const urlVnEx = "https://vnexpress.net";

const pageSize = 10;

export { noloadEnum, urlVnEx, pageSize };
