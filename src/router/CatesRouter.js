import CatesController from "../controller/CatesController.js";
import express from "express";

export function initRouterCates() {
  const catesController = new CatesController();
  console.log(catesController)
  const router = express.Router();
  router.get("/", catesController.findAll);
  router.get("/:idCates", catesController.findById);
  return router;
}
