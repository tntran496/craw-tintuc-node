import { Sequelize } from "sequelize";
const sequelize = new Sequelize(
  process.env.DATABASE_NAME || "mycrawl",
  process.env.DATABASE_USER || "user",
  process.env.DATABASE_PASSWORD || "123456",
  {
    host: process.env.DATABASE_HOST || "localhost",
    port: process.env.DATABASE_PORT || 3306,
    dialect: "mysql",
    pool: {
      max: 200,
      min: 0,
      idle: 10000,
    },
    define: {
      charset: "utf8",
      collate: "utf8_general_ci",
    },
  }
);
export { sequelize };
