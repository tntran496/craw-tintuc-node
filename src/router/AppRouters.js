import * as vnNewRouter from "./NewsRouter.js";
import * as vnCateRouter from "./CatesRouter.js";
import express from 'express';

export function initRouters(app) {
  
  app.use('/api/v1/vnexpress/news/',vnNewRouter.initRoutersNews());
  app.use('/api/v1/vnexpress/cates/',vnCateRouter.initRouterCates());
}