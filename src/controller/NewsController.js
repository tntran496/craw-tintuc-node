import NewsRepo from "../repository/NewsRepo.js";
import { pageSize } from "../constant/Constant.js";

export default class NewsController {
  constructor() {
    this.newsRepo = new NewsRepo();
  }
  FindByCate = async (req, res, next) => {
    try {
      const { cateId, page } = req.query;
      const pagination = {
        limit: pageSize,
        offset: pageSize * page - pageSize,
      };
      const results = await this.newsRepo.findByCate(cateId, pagination);
      res.status(200).send(results);
    } catch (error) {
      next(error);
    }
  }
  findByID = async (req, res, next) => {
    try {
      const { id } = req.params;
      const results = await this.newsRepo.findById(id);
      res.status(200).send(results);
    } catch (error) {
      next(error);
    }
  }
  findByTitle = async (req, res, next) => {
    try {
      const { title, page } = req.query;
      const pagination = {
        limit: pageSize,
        offset: pageSize * page - pageSize,
      };
      const results = await this.newsRepo.findByTitle(title, pagination);
      res.status(200).send(results);
    } catch (error) {
      next(error);
    }
  }
}
