import CatesRepo from "../repository/CatesRepo.js";

export default class CatesController {
  constructor() {
    this.catesRepo = new CatesRepo();
  }
  findAll = async (req, res, next) => {
    try {
      console.log(this)
      const results = await this.catesRepo.findAll();
      res.status(200).send(results);
    } catch (error) {
      next(error);
    }
  }
  findById = async (req, res, next) => {
    try {
      const { idCates } = req.params;
      const result = await this.catesRepo.findById(idCates);
      res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  }
}
