import NewsController from '../controller/NewsController.js'
import express from "express";

export function initRoutersNews(){
    const newsController = new NewsController()
    console.log(newsController)
    const router = express.Router();
    router.get('/search_tille', newsController.findByTitle)
    router.get('/search_cate', newsController.FindByCate)
    router.get('/:id', newsController.findByID)
    return router;
}
