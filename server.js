import env from "dotenv";
env.config();

import express from "express";
import cookieParser from "cookie-parser";
import logger from "morgan";
import http from "http";
import cors from "cors";

import * as routers from "./src/router/AppRouters.js";
import "./src/service/NewService.js";
import "./src/service/CateService.js";

const port = process.env.PORT_SERVER || 5000;
const app = express();
const server = http.createServer(app);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500);
  res.send({
    status: error.status || 500,
    message: error.message || "Internal Server Error",
  });
});

routers.initRouters(app);

server.listen(port);
