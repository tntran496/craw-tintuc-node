# RESTAPI CRAW NEW IN VNEXPRESS
## Clone the repository
```
git clone https://gitlab.com/tntran496/craw-tintuc-node.git
```
## Run project with docker-compose (Need install docker and docker-compose)
```bash
cd /craw-tintuc-node
docker-compose up -d
```
## API Document endpoints

  swagger Spec Endpoint : http://localhost:3000/api-docs 
