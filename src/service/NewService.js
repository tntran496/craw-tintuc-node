import cheerio from "cheerio";
import request from "request-promise";
import Cates from "../model/CatesModel.js";
import NewsInfos from "../model/NewsModel.js";
import { urlVnEx } from "../constant/Constant.js";

const h = 60 * 60 * 1 * 1000;
const s = 1000;

let crawNews = async () => {
  try {
    let arrCate = await Cates.findAll();
    if (arrCate.length > 0) {
      for (let item of arrCate) {
        const htmlSub = await request.get(`${urlVnEx}${item.url}`);
        console.log(`${urlVnEx}${item.url}`);
        let $ = cheerio.load(htmlSub.toString());
        $("article").each(async (i, e) => {
          let title = $(e).find(".title-news").text();
          let description = $(e).find(".description").text().trim();
          let thumbnail = $(e).find("img").attr("data-src");

          let urlDetail = $(e).find("a").attr("href");
          let detail = ''
          if(typeof urlDetail != "undefined"){
            const htmlDetail = await request.get(`${urlDetail}`);
            let $2 = cheerio.load(htmlDetail.toString());
            $2(".fck_detail ").each(async (i2,e2)=>{
              let row = $2(e2).find('.Normal').text()
              detail = detail.concat(row)
            })
          }
          if (
            title != "" &&
            description != "" &&
            typeof thumbnail != "undefined"
          ) {
            await NewsInfos.create({
              title: title,
              description: description,
              thumbnail: thumbnail,
              detail: detail,
              CateId: item.id,
            });
          }
        });
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};
setTimeout(crawNews, 2 * s);
setInterval(crawNews, 24 * h);
