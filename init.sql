use mycrawl;
ALTER DATABASE mycrawl CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table Cates(
    id varchar(255),
    name text,
    url varchar(255),
    primary key (id)
);
ALTER TABLE Cates CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table NewsInfos(
    id serial,
    title text,
    description text,
    thumbnail varchar(255),
    CateId varchar(255),
    detail text,
    createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
    primary key (id),
    constraint fk_Cates_News foreign key (CateId) references Cates(id)
);
ALTER TABLE NewsInfos CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;